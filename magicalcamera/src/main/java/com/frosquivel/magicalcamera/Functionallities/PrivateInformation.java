package com.frosquivel.magicalcamera.Functionallities;

import com.frosquivel.magicalcamera.Objects.PrivateInformationObject;
import ohos.media.image.ExifUtils;
import ohos.media.image.ImageSource;

/**
 * Created by Fabian Rosales Esquivel (Frosquivel Developer)
 * Created Date 07/12/2016.
 * Made in Costa Rica
 * This class access to information of photographies with exif interface
 */

public class PrivateInformation {

    //properties
    private PrivateInformationObject privateInformationObject;

    public PrivateInformationObject getPrivateInformationObject() {
        return privateInformationObject;
    }

    //constructor
    public PrivateInformation() {
        super();
        privateInformationObject = new PrivateInformationObject();
    }

    //================================================================================
    // Exif interface methods
    //================================================================================
    private ImageSource getAllFeatures(String realPath) {
        if (!realPath.equals("")) {
            ImageSource exif = null;
            exif = ImageSource.create(realPath, null);
            return exif;
        } else {
            return null;
        }
    }

    public boolean getImageInformation(String realPath) {
        try {
            ImageSource exif = getAllFeatures(realPath);
            if (exif != null) {
                privateInformationObject.setLatitude(ExifUtils.getLatLong(exif).f);
                privateInformationObject.setLongitude(ExifUtils.getLatLong(exif).s);
                privateInformationObject.setImageWidth(String.valueOf(exif.getImageInfo().size.width));
                privateInformationObject.setImageHeight(String.valueOf(exif.getImageInfo().size.height));
                privateInformationObject.setAltitude(ExifUtils.getAltitude(exif, 0));

                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }
    public boolean getImageInformation(ImageSource exif) {
        try {
            if (exif != null) {

                privateInformationObject.setLatitude(ExifUtils.getLatLong(exif)!=null?ExifUtils.getLatLong(exif).f:0);
                privateInformationObject.setLongitude(ExifUtils.getLatLong(exif)!=null?ExifUtils.getLatLong(exif).s:0);
                privateInformationObject.setImageWidth(String.valueOf(exif.getImageInfo().size.width));
                privateInformationObject.setImageHeight(String.valueOf(exif.getImageInfo().size.height));
                privateInformationObject.setAltitude(ExifUtils.getAltitude(exif, 0));

                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }
    }
}
