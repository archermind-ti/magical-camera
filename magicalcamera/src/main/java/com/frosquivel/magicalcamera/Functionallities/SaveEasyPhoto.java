package com.frosquivel.magicalcamera.Functionallities;


import com.frosquivel.magicalcamera.MagicalCamera;
import ohos.aafwk.ability.Ability;
import ohos.app.Environment;
import ohos.media.image.ImagePacker;
import ohos.media.image.PixelMap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Fabian Rosales Esquivel (Frosquivel Developer)
 * Created Date 07/12/2016.
 * Made in Costa Rica
 * This class save the photo bitmap like a real photo in device
 */

public class SaveEasyPhoto {

    //================================================================================
    // Save Photo in device
    //================================================================================
    //region Save Photo in device

    /**
     * This library write the file in the device storage or sdcard
     *
     * @param bitmap                  the bitmap that you need to write in device
     * @param photoName               the photo name
     * @param directoryName           the directory that you need to create the picture
     * @param format                  the format of the photo, maybe png or jpeg
     * @param autoIncrementNameByDate is this variable is active the system create
     *                                the photo with a number of the date, hour, and second to diferenciate this
     * @return return true if the photo is writen
     */
    public String writePhotoFile(PixelMap bitmap, String photoName, String directoryName,
                                 String format, boolean autoIncrementNameByDate, Ability activity) {

        if (bitmap == null) {
            return null;
        } else {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();

            ImagePacker imagePacker = ImagePacker.create();
            ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
            packingOptions.quality = 100;
            boolean result =  imagePacker.initializePacking(bytes,packingOptions);
            result = imagePacker.addImage(bitmap);
            imagePacker.finalizePacking();

            DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
            String date = df.format(Calendar.getInstance().getTime());

            if (format == MagicalCamera.PNG) {
                photoName = (autoIncrementNameByDate) ? photoName + "_" + date + ".png" : photoName + ".png";
            } else if (format == MagicalCamera.JPEG) {
                photoName = (autoIncrementNameByDate) ? photoName + "_" + date + ".jpeg" : photoName + ".jpeg";
            } else if (format == MagicalCamera.WEBP) {
                photoName = (autoIncrementNameByDate) ? photoName + "_" + date + ".webp" : photoName + ".webp";
            }

            File wallpaperDirectory = null;

            try {
                wallpaperDirectory = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES + "/" + directoryName + "/");
            } catch (Exception ev) {
            }

            if (wallpaperDirectory != null) {
                if (!wallpaperDirectory.exists()) {
                    wallpaperDirectory.exists();
                    wallpaperDirectory.mkdirs();
                }

                File f = new File(wallpaperDirectory, photoName);
                try {
                    f.createNewFile();
                    FileOutputStream fo = new FileOutputStream(f);
                    fo.write(bytes.toByteArray());
                    fo.close();

                    return f.getAbsolutePath();
                } catch (Exception ev) {
                    return null;
                }

            } else {
                return null;
            }
        }
    }
    //endregion
}
