package com.frosquivel.magicalcamera.Functionallities;


import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.Objects.ActionPictureObject;
import com.frosquivel.magicalcamera.Utilities.PictureUtils;
import com.frosquivel.magicalcamera.ui.CameraAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;


/**
 * Created by Fabian Rosales Esquivel (Frosquivel Developer)
 * Created Date 07/12/2016.
 * Made in Costa Rica
 * This class call the intent of take picture or select picture in device
 */

public class ActionPicture {

    //================================================================================
    // Properties and constructor
    //================================================================================
    //region Properties
    private ActionPictureObject actionPictureObject;
    private URIPaths uriPaths;

    //Getter and Setter methods
    public ActionPictureObject getActionPictureObject() {
        return actionPictureObject;
    }
    //endregion

    //region Constructor
    public ActionPicture(AbilitySlice activity, int resizePicture, URIPaths uriPaths) {
        this.actionPictureObject = new ActionPictureObject();
        this.uriPaths = uriPaths;
        this.actionPictureObject.setActivity(activity);
        this.actionPictureObject.setResizePhoto(resizePicture);
    }
    //endregion


    //================================================================================
    // Take and Select photos
    //================================================================================
    //region Photo Methods

    /**
     * This method call the intent for take the picture in activity screen
     *
     * @return return true if the picture was taken
     */
    public boolean takePhoto() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getActionPictureObject().getActivity().getBundleName())
                .withAbilityName(CameraAbility.class.getName())
                .build();
        intent.setOperation(operation);
        getActionPictureObject().getActivity().startAbilityForResult(intent, MagicalCamera.TAKE_PHOTO);
        return true;
    }


    /**
     * This call the intent to selected the picture for activity screen
     *
     * @param headerName the header name of popUp that you need to shown
     * @return return true if the photo was taken or false if it was not.
     */
    public boolean selectedPicture(String headerName) {

        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.PICK");
            intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
            intent.setType("image/*");
            this.actionPictureObject.getActivity().startAbilityForResult(
                    intent,
                    MagicalCamera.SELECT_PHOTO);

            return true;
        } catch (Exception ev) {
            return false;
        }
    }

    /**
     * This method obtain the path of the picture selected, and convert this in the
     * phsysical path of the image, and decode the file with the respective options,
     * resize the file and change the quality of photos selected.
     *
     * @param data the intent data for take the photo path
     * @return return a bitmap of the photo selected
     */
    @SuppressWarnings("deprecation")
    private PixelMap onSelectFromGalleryResult(Intent data) {

        DataAbilityHelper helper = DataAbilityHelper.creator(getActionPictureObject().getActivity());
        PixelMap bm = null;
        ImageSource imageSource = null;
        try {
            String id = data.getUri().getDecodedPath().substring(data.getUri().getDecodedPath().lastIndexOf("/") + 1);
            Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, id);
            FileDescriptor fileDescriptor = helper.openFile(uri, "r");
            imageSource = ImageSource.create(fileDescriptor, null);
            ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
            bm = imageSource.createPixelmap(options);
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            e.printStackTrace();
        }
        bm = PictureUtils.resizePhoto(bm, this.actionPictureObject.getResizePhoto(), true);
        this.uriPaths.getPhotoFileUri(imageSource, data.getUri());
        if (bm != null)
            return bm;
        else
            return null;
    }

    /**
     * Save the photo in memory bitmap, resize and return the photo
     *
     * @return the bitmap of the respective photo
     */
    private PixelMap onTakePhotoResult(Intent data) {
        // by this point we have the camera photo on disk
        if (data.getUri() != null) {
            ImageSource imageSourceNoOptions = ImageSource.create(data.getUri().getDecodedPath(), null);
            this.uriPaths.getUriPathsObject().setImageSource(imageSourceNoOptions);
            PixelMap takenImage = imageSourceNoOptions.createPixelmap(null);
            takenImage = PictureUtils.resizePhoto(takenImage, actionPictureObject.getResizePhoto(), true);
            return takenImage;
        } else {
            return null;
        }
    }
    //endregion

    //region Method to call in Override

    /**
     * This methods is called in the override method onActivityResult
     * for the respective activation, and this validate which of the intentn result be,
     * for example: if is selected file or if is take picture
     */
    public void resultPhoto(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (requestCode == MagicalCamera.SELECT_PHOTO) {
                this.actionPictureObject.setMyPhoto(onSelectFromGalleryResult(data));
            } else if (requestCode == MagicalCamera.TAKE_PHOTO) {
                this.actionPictureObject.setMyPhoto(onTakePhotoResult(data));
            }
        }
    }

    /**
     * This methods is called in the override method onActivityResult
     * for the respective activation, and this validate which of the intentn result be,
     * for example: if is selected file or if is take picture
     * <p>
     * doLandScape
     * BUT you have the posibillity of rotate the picture "manually", with the parameter doLandScape
     */
    public void resultPhoto(int requestCode, int resultCode, Intent data, int rotatePicture) {
        if (resultCode == -1) {
            if (requestCode == MagicalCamera.SELECT_PHOTO) {
                this.actionPictureObject.setMyPhoto(onSelectFromGalleryResult(data));
            } else if (requestCode == MagicalCamera.TAKE_PHOTO) {
                this.actionPictureObject.setMyPhoto(onTakePhotoResult(data));
            }

            if (this.actionPictureObject.getMyPhoto() != null) {
                this.actionPictureObject.setMyPhoto(PictureUtils.rotateImage(this.uriPaths.getUriPathsObject().getImageSource(), rotatePicture));
            }
        }
    }
    //endregion
}
