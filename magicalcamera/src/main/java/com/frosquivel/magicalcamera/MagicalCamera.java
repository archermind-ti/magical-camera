package com.frosquivel.magicalcamera;


import com.frosquivel.magicalcamera.Objects.ActionPictureObject;
import com.frosquivel.magicalcamera.Objects.MagicalCameraObject;
import com.frosquivel.magicalcamera.Objects.PrivateInformationObject;
import com.frosquivel.magicalcamera.Utilities.PictureUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;


/**
 * Created by          Fabián Rosales Esquivel
 * Created Date        on 5/15/16
 * This have the objects or anothers classes to toast the best form to use this library
 * I recommended use only this class in your code for take a best experience of this third party library
 * You have the possibility of write me in my personal email fabian7593@gmail.com
 * v2
 */
public class MagicalCamera {

    //compress format public static variables
    public static final String JPEG = "jpeg";
    public static final String PNG = "png";
    public static final String WEBP = "webp";

    //the orientation rotates
    public static final int ORIENTATION_ROTATE_NORMAL = 0;
    public static final int ORIENTATION_ROTATE_90 = 90;
    public static final int ORIENTATION_ROTATE_180 = 180;
    public static final int ORIENTATION_ROTATE_270 = 270;

    //other constants for realized tasks
    public static final int TAKE_PHOTO = 0;
    public static final int SELECT_PHOTO = 1;
    public static final int LANDSCAPE_CAMERA = 2;
    public static final int NORMAL_CAMERA = 3;

    //Constants for permissions
    public static final String CAMERA = "ohos.permission.CAMERA";
    public static final String EXTERNAL_STORAGE = "ohos.permission.READ_MEDIA";

    private MagicalCameraObject magicalCameraObject;
    private MagicalPermissions magicalPermissions;

    //================================================================================
    // Constructs
    //================================================================================
    //region Construct
    public MagicalCamera(AbilitySlice activity, int resizePhoto, MagicalPermissions magicalPermissions) {
        resizePhoto = resizePhoto <= 0 ? ActionPictureObject.BEST_QUALITY_PHOTO : resizePhoto;
        magicalCameraObject = new MagicalCameraObject(activity, resizePhoto);
        this.magicalPermissions = magicalPermissions;
    }
    //endregion


    //Principal Methods
    public void takePhoto(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                magicalCameraObject.getActionPicture().takePhoto();
            }
        };
        askPermissions(runnable, CAMERA);
    }

    public void selectedPicture(final String headerPopUpName){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                magicalCameraObject.getActionPicture().selectedPicture(headerPopUpName);
            }
        };
        askPermissions(runnable, EXTERNAL_STORAGE);
    }

    private void askPermissions(final Runnable runnable, final String operationType){
        magicalPermissions.askPermissions(runnable, operationType);
    }


    //Image information methods
    public boolean initImageInformation() {
        return magicalCameraObject.getPrivateInformation().getImageInformation(magicalCameraObject.getUriPaths().getUriPathsObject().getImageSource());
    }

    public PrivateInformationObject getPrivateInformation() {
        return magicalCameraObject.getPrivateInformation().getPrivateInformationObject();
    }

    /**
     * ***********************************************
     * This methods save the photo in memory device
     * with diferents params
     * **********************************************
     */
    public String savePhotoInMemoryDevice(PixelMap bitmap, String photoName, boolean autoIncrementNameByDate) {
        return magicalCameraObject.getSaveEasyPhoto().writePhotoFile(bitmap,
                photoName, "MAGICAL CAMERA", PNG, autoIncrementNameByDate, magicalCameraObject.getActivity().getAbility());
    }

    public String savePhotoInMemoryDevice(PixelMap bitmap, String photoName, String format, boolean autoIncrementNameByDate) {
        return magicalCameraObject.getSaveEasyPhoto().writePhotoFile(bitmap,
                photoName, "MAGICAL CAMERA", format, autoIncrementNameByDate, magicalCameraObject.getActivity().getAbility());
    }

    public String savePhotoInMemoryDeviceWithDirectory(PixelMap bitmap, String photoName, String directoryName, boolean autoIncrementNameByDate) {
        return magicalCameraObject.getSaveEasyPhoto().writePhotoFile(bitmap,
                photoName, directoryName, PNG, autoIncrementNameByDate, magicalCameraObject.getActivity().getAbility());
    }

    public String savePhotoInMemoryDevice(PixelMap bitmap, String photoName, String directoryName,
                                           String format, boolean autoIncrementNameByDate) {
        return magicalCameraObject.getSaveEasyPhoto().writePhotoFile(bitmap,
                photoName, directoryName, format, autoIncrementNameByDate, magicalCameraObject.getActivity().getAbility());
    }

    //get variables
    public String getRealPath(){
        return magicalCameraObject.getUriPaths().getUriPathsObject().getRealPath();
    }

    public PixelMap getPhoto(){
        return magicalCameraObject.getActionPicture().getActionPictureObject().getMyPhoto();
    }
    public ImageSource getPhotoImageSource(){
        return magicalCameraObject.getUriPaths().getUriPathsObject().getImageSource();
    }

    public void setPhoto(PixelMap bitmap){
        magicalCameraObject.getActionPicture().getActionPictureObject().setMyPhoto(bitmap);
    }
    public void setPhotoImageSource(ImageSource source){
        magicalCameraObject.getUriPaths().getUriPathsObject().setImageSource(source);
    }

    public void resultPhoto(int requestCode, int resultCode, Intent data){
        magicalCameraObject.getActionPicture().resultPhoto(requestCode, resultCode, data);
    }

    public void resultPhoto(int requestCode, int resultCode, Intent data, int rotateType){
        magicalCameraObject.getActionPicture().resultPhoto(requestCode, resultCode, data, rotateType);
    }

    public Intent getIntentFragment(){
        return magicalCameraObject.getActionPicture().getActionPictureObject().getIntentFragment();
    }
    public void setResizePhoto(int resize){
         magicalCameraObject.getActionPicture().getActionPictureObject().setResizePhoto(resize);
    }


    public PixelMap rotatePicture(ImageSource source,int rotateType){
        if(source != null)
            return PictureUtils.rotateImage(source, rotateType);
        else
            return null;
    }

    //endregion
}
