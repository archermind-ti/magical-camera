package com.frosquivel.magicalcamera.Utilities;


import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.ByteArrayOutputStream;
import java.util.Base64;

/**
 * Created by Fabian on 07/12/2016.
 */

public class ConvertSimpleImage {

    //================================================================================
    // Conversion Methods
    //================================================================================

    public static PixelMap resizeImageRunTime(byte[] byteArray, int width, int height, boolean isFilter) {
        ImageSource source = ImageSource.create(byteArray, null);
        // 普通解码叠加缩放、裁剪、旋转
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(width, height);
        return source.createPixelmap(decodingOpts);
    }

    //region Conversion Methods
    public static byte[] bitmapToBytes(PixelMap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImagePacker imagePacker = ImagePacker.create();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.quality = 100;
        imagePacker.initializePacking(stream, packingOptions);
        imagePacker.addImage(bitmap);
        imagePacker.finalizePacking();
        return stream.toByteArray();
    }

    public static PixelMap bytesToBitmap(byte[] byteArray) {
        ImageSource source = ImageSource.create(byteArray, null);
        return source.createPixelmap(null);
    }


    public static String bytesToStringBase64(byte[] byteArray) {
        StringBuilder base64 = new StringBuilder(Base64.getEncoder().encodeToString(byteArray));
        return base64.toString();
    }

    public static byte[] stringBase64ToBytes(String stringBase64) {
        byte[] byteArray = Base64.getDecoder().decode(stringBase64);
        return byteArray;
    }
    //endregion
}
