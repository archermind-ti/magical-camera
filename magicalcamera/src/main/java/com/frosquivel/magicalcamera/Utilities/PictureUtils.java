package com.frosquivel.magicalcamera.Utilities;


import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

/**
 * Created by Fabian on 08/12/2016.
 */

public class PictureUtils {
    //===============================================================================
    // Utils methods, resize and get Photo Uri and others
    //================================================================================

    /**
     * Rotate the bitmap if the image is in landscape camera
     *
     * @param source
     * @param angle
     * @return
     */
    public static PixelMap rotateImage(ImageSource source, float angle) {
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.rotateDegrees =angle;
        return source.createPixelmap(decodingOpts);
    }

    /**
     * This method resize the photo
     *
     * @param realImage    the bitmap of image
     * @param maxImageSize the max image size percentage
     * @param filter       the filter
     * @return a bitmap of the photo rezise
     */
    public static PixelMap resizePhoto(PixelMap realImage, float maxImageSize,
                                       boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getImageInfo().size.width,
                (float) maxImageSize / realImage.getImageInfo().size.height);
        int width = Math.round((float) ratio * realImage.getImageInfo().size.width);
        int height = Math.round((float) ratio * realImage.getImageInfo().size.height);
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(width,height);
        return PixelMap.create(realImage,options);
    }
}
