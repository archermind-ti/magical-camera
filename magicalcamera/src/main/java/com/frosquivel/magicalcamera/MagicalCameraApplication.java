package com.frosquivel.magicalcamera;

import ohos.aafwk.ability.AbilityPackage;

/**
 * Created by Fabian on 23/02/2019.
 */

public class MagicalCameraApplication extends AbilityPackage {

    // Called when the application is starting, before any other application objects have been created.
    // Overriding this method is totally optional!
    @Override
    public void onInitialize() {
        super.onInitialize();
        // Required initialization logic here!
    }
}
