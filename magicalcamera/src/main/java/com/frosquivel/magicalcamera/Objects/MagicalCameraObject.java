package com.frosquivel.magicalcamera.Objects;


import com.frosquivel.magicalcamera.Functionallities.ActionPicture;
import com.frosquivel.magicalcamera.Functionallities.PrivateInformation;
import com.frosquivel.magicalcamera.Functionallities.SaveEasyPhoto;
import com.frosquivel.magicalcamera.Functionallities.URIPaths;
import ohos.aafwk.ability.AbilitySlice;

/**
 * Created by Fabian on 07/12/2016.
 */

public class MagicalCameraObject {
    //================================================================================
    // Properties
    //================================================================================

    //my activity variable
    private AbilitySlice activity;


    //the private information class for instance
    private PrivateInformation privateInformation;

    //the private variable of saveEasyPhoto
    private SaveEasyPhoto saveEasyPhoto;

    //the actions to take pictures or selected
    private ActionPicture actionPicture;

    //the uri of paths class
    private URIPaths uriPaths;
    //endregion


    //Constructor
    public MagicalCameraObject(AbilitySlice activity, int qualityPhoto){
        this.activity = activity;
        this.privateInformation = new PrivateInformation();
        this.uriPaths = new URIPaths(this.privateInformation, activity);
        this.saveEasyPhoto = new SaveEasyPhoto();
        this.actionPicture = new ActionPicture(activity, qualityPhoto, this.uriPaths);
    }


    //================================================================================
    // Accessors
    //================================================================================

    public AbilitySlice getActivity() {
        return activity;
    }

    public void setActivity(AbilitySlice activity) {
        this.activity = activity;
    }

    public PrivateInformation getPrivateInformation() {
        return privateInformation;
    }

    public void setPrivateInformation(PrivateInformation privateInformation) {
        this.privateInformation = privateInformation;
    }

    public SaveEasyPhoto getSaveEasyPhoto() {
        return saveEasyPhoto;
    }

    public void setSaveEasyPhoto(SaveEasyPhoto saveEasyPhoto) {
        this.saveEasyPhoto = saveEasyPhoto;
    }

    public ActionPicture getActionPicture() {
        return actionPicture;
    }

    public void setActionPicture(ActionPicture actionPicture) {
        this.actionPicture = actionPicture;
    }
    public URIPaths getUriPaths() {
        return uriPaths;
    }
    //endregion
}
