package com.frosquivel.magicalcamera.Objects;


import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.media.image.PixelMap;

/**
 * Created by Fabian on 08/12/2016.
 */

public class ActionPictureObject {

    //the max of quality photo
    public static final int BEST_QUALITY_PHOTO = 4000;

    //The photo name for uxiliar save picture
    public static final String photoNameAuxiliar = "MagicalCamera";

    //properties
    //my intent curret fragment (only use for fragments)
    private Intent intentFragment;

    //Your own resize picture
    private int resizePhoto;

    //my activity variable
    private AbilitySlice activity;

    //bitmap to set and get
    private PixelMap myPhoto;


    //getters and setters
    public Intent getIntentFragment() {
        return intentFragment;
    }

    public void setIntentFragment(Intent intentFragment) {
        this.intentFragment = intentFragment;
    }

    public PixelMap getMyPhoto() {
        return myPhoto;
    }

    public void setMyPhoto(PixelMap myPhoto) {
        this.myPhoto = myPhoto;
    }

    public int getResizePhoto() {
        return resizePhoto;
    }

    public void setResizePhoto(int resizePhoto) {
        resizePhoto = resizePhoto * 40;
        if (resizePhoto <= BEST_QUALITY_PHOTO && resizePhoto > 0)
            this.resizePhoto = resizePhoto;
        else{
            this.resizePhoto = BEST_QUALITY_PHOTO;
        }
    }

    public AbilitySlice getActivity() {
        return activity;
    }

    public void setActivity(AbilitySlice activity) {
        this.activity = activity;
    }
}
