package com.frosquivel.magicalcamera.Objects;

import ohos.media.image.ImageSource;

/**
 * Created by Fabian on 08/12/2016.
 */

public class URIPathsObject {
    //THIS IS THE REAL PATH IN DEVICE
    private String realPath;
    private ImageSource imageSource;

    public ImageSource getImageSource() {
        return imageSource;
    }

    public void setImageSource(ImageSource imageSource) {
        this.imageSource = imageSource;
    }

    public String getRealPath() {
        return realPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }
}
