# MagicalCamera

### 简介

在 OHOS 中拍照和选择图片的魔法库。 方法很简单，如果需要也可以把图片保存在设备中，获取真实的uri路径或者图片或者获取图片的私密信息。

### 功能
- 拍照获取图片
- 通过相册获取图片
- 保存选择图片，可以选择jpeg、png、web格式
- 设置图片大小
- 获取图片base64编码
- 旋转图片


### 演示

![image](screenshot/sample.gif)

### 集成

在project的build.gradle中添加mavenCentral()的引用

```
repositories {   
 	...   
 	mavenCentral()   
	 ...           
 }
```

在entry的build.gradle中添加依赖

```
dependencies { 
... 
implementation 'com.gitee.archermind-ti:magicalcamera:1.0.2' 
... 
}
```

### 使用说明

- MagicalCamera 添加相机、存储权限

```
String[] permissions = new String[] {
            "ohos.permission.CAMERA",
            "ohos.permission.WRITE_EXTERNAL_STORAGE",
            "ohos.permission.READ_EXTERNAL_STORAGE",
    };
magicalPermissions = new MagicalPermissions(this, permissions);

```
- 创建 MagicalCamera
```
MagicalCamera magicalCamera = new MagicalCamera(this,RESIZE_PHOTO_PIXELS_PERCENTAGE, magicalPermissions);
```
```
//take photo
magicalCamera.takePhoto();

//select picture
magicalCamera.selectedPicture("my_header_name");

```
```
//在onAbilityResult里获取选择图片数据
    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultCode != -1 || resultData == null) {
            return;
        }
        if (requestCode == MagicalCamera.SELECT_PHOTO) {
            magicalCamera.resultPhoto(requestCode, resultCode, resultData);
            image.setPixelMap(magicalCamera.getPhoto());
            btnSavePhoto.setVisibility(Component.VISIBLE);
            btnMenu.setVisibility(Component.VISIBLE);
        }

    }
```
```
//重新设置图片质量
magicalCamera.setResizePhoto(newResizeInteger);
```
- 转换方法

该库有方法可以将位图转换为您需要的其他格式。 所有这些方法都是公共静态，我的意思是你不必实例化库来使用它。 您需要调用类 * ConvertSimpleImage * 和相应的参数。
  1. bitmapToBytes：将位图转换为数组字节，只需要位图参数和压缩格式，返回数组字节。
  2. bytesToBitmap：将数组字节转换为位图，只需要param中的数组字节，返回位图。
  3. bytesToStringBase64：将数组字节转换为64进制的字符串，只需要param中的数组字节格式，返回String。
  4. stringBase64ToBytes：将字符串转换为数组字节，只需要param中的String，返回数组字节。
```
//convert the bitmap to bytes
  byte[] bytesArray =  ConvertSimpleImage.bitmapToBytes(magicalCamera.getPhoto(), MagicalCamera.PNG);
  
   //convert the bytes to string 64, with this form is easly to send by web service or store data in DB
   String imageBase64 = ConvertSimpleImage.bytesToStringBase64(bytesArray);

   //if you need to revert the process
   byte[] anotherArrayBytes = ConvertSimpleImage.stringBase64ToBytes(imageBase64);

  //again deserialize the image
  Bitmap myImageAgain = ConvertSimpleImage.bytesToBitmap(anotherArrayBytes);
```
### 版本迭代
[ChangeLog](changelog.md)
- v1.0.0
- v1.0.1
- v1.0.2

### 编译说明

1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击`Run`运行即可（真机运行可能需要配置签名）

### 版权和许可信息
Copyright 2016 Fabian Rosales

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
<br><br>

