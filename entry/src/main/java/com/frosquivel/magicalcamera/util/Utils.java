package com.frosquivel.magicalcamera.util;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.media.image.PixelMap;

/**
 * Created by Fabian on 01/03/2017.
 * This class have the general method for avoid code duplicate
 */

public class Utils {

    public static final String C_PREFERENCE_MAGICAL_CAMERA = "MCPreference";
    public static final String C_PREFERENCE_MC_DIRECTORY_NAME = "MCDirectoryName";
    public static final String C_PREFERENCE_MC_PHOTO_NAME = "MCPhotoName";
    public static final String C_PREFERENCE_MC_SELECTED_PICTURE = "MCTitleSelectPicture";
    public static final String C_PREFERENCE_MC_QUALITY_PICTURE = "MCQualityPicture";
    public static final String C_PREFERENCE_MC_FORMAT = "MCFormat";
    public static final String C_PREFERENCE_MC_AUTO_IC_NAME = "MCAutoincrementName";
    public static final String C_PREFERENCE_MC_FACIAL_RECOGNITION_THICK = "MCFacialRecognitionThick";
    public static final String C_PREFERENCE_MC_FACIAL_RECOGNITION_COLOR = "MCFacialRecognitionColor";

    public static final String C_PNG = "png";
    public static final String C_JPEG = "jpeg";
    public static final String C_WEBP = "webp";

    public static PixelMap magicalCameraBitmap;


    /**
     * validate if the variable is null or empty
     *
     * @param validate the string to validate
     * @return true or false
     */
    public static boolean notNullNotFill(String validate) {
        if (validate != null) {
            if (!validate.trim().equals("")) {
                return true;
            } else {

                return false;
            }
        } else {
            return false;
        }
    }


    //#Shared preference methods
    public static void setSharedPreference(Context context, String preferenceName, String preferenceValue) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context.getApplicationContext()); // context入参类型为ohos.app.Context。
        String fileName = C_PREFERENCE_MAGICAL_CAMERA; // fileName表示文件名，其取值不能为空，也不能包含路径，默认存储目录可以通过context.getPreferencesDir()获取。
        Preferences preferences = databaseHelper.getPreferences(fileName);
        preferences.putString(preferenceName, preferenceValue);
        preferences.flush();

    }

    public static String getSharedPreference(Context context, String preferenceName) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context.getApplicationContext()); // context入参类型为ohos.app.Context。
        String fileName = C_PREFERENCE_MAGICAL_CAMERA; // fileName表示文件名，其取值不能为空，也不能包含路径，默认存储目录可以通过context.getPreferencesDir()获取。
        Preferences preferences = databaseHelper.getPreferences(fileName);
        return preferences.getString(preferenceName, "");
    }

    //initial the shared preference
    public static void setInitialSharedPreference(Context context, boolean isDefaultValues) {
        context = context.getApplicationContext();

        if (getSharedPreference(context, C_PREFERENCE_MC_DIRECTORY_NAME).equals("") || isDefaultValues)
            setSharedPreference(context, C_PREFERENCE_MC_DIRECTORY_NAME, "Magical Camera");

        if (getSharedPreference(context, C_PREFERENCE_MC_PHOTO_NAME).equals("") || isDefaultValues)
            setSharedPreference(context, C_PREFERENCE_MC_PHOTO_NAME, "MagicalCamera_test_photo_name");

        if (getSharedPreference(context, C_PREFERENCE_MC_QUALITY_PICTURE).equals("") || isDefaultValues)
            setSharedPreference(context, C_PREFERENCE_MC_QUALITY_PICTURE, "100");

        if (getSharedPreference(context, C_PREFERENCE_MC_FORMAT).equals("") || isDefaultValues)
            setSharedPreference(context, C_PREFERENCE_MC_FORMAT, C_JPEG);

        if (getSharedPreference(context, C_PREFERENCE_MC_AUTO_IC_NAME).equals("") || isDefaultValues)
            setSharedPreference(context, C_PREFERENCE_MC_AUTO_IC_NAME, "True");

        if (getSharedPreference(context, C_PREFERENCE_MC_FACIAL_RECOGNITION_THICK).equals("") || isDefaultValues)
            setSharedPreference(context, C_PREFERENCE_MC_FACIAL_RECOGNITION_THICK, "50");

    }


//    public static void openScheme(Activity activity, String scheme, String id, String url, String errorMessage){
//        Uri uri = Uri.parse(scheme);
//        try {
//            uri = ContentUris.withAppendedId(uri, Long.parseLong(id));
//        }catch(Exception ev){
//            uri = Uri.parse(scheme + id);
//        }
//        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        try {
//            activity.startActivity(intent);
//        }catch(Exception ev){
//            try {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//                activity.startActivity(browserIntent);
//            }catch(Exception ex){
//                Toast.makeText(activity,errorMessage,Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//
//    public static void sendMeAnEmail(Ability activity){
//        Intent email = new Intent(Intent.ACTION_SEND);
//        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"fabian7593@gmail.com"});
//        email.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.email_subject));
//        email.putExtra(Intent.EXTRA_TEXT, "");
//        email.setType("message/rfc822");
//        activity.startActivity(Intent.createChooser(email, activity.getString(R.string.email_choose)));
//    }
//
    public static void sharedApp(Ability activity) {
//        Intent shareIntent = new Intent();
//        String textEmail = activity.getString(R.string.email_text);
//        textEmail = textEmail.replace("XXXX1", activity.getString(R.string.link_git));
//        textEmail = textEmail.replace("XXXX2", activity.getString(R.string.link_play_store));
//        textEmail = textEmail.replace("XXXX3", activity.getString(R.string.link_play_store_payment));
//
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.setType("text/plain");
//        shareIntent.putExtra(Intent.EXTRA_TEXT, textEmail);
//        shareIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.email_subject));
//        shareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        activity.startActivity(Intent.createChooser(shareIntent, activity.getString(R.string.email_title)));
    }
}
