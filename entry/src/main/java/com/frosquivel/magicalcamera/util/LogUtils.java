package com.frosquivel.magicalcamera.util;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class LogUtils {
    public static void LogInfo(String tag, String words){
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.DEBUG,0,tag);
        HiLog.info(hiLogLabel,words);
    }
    public static void LogDebug(String tag, String words){
        HiLogLabel hiLogLabel = new HiLogLabel(HiLog.DEBUG,0,tag);
        HiLog.debug(hiLogLabel,words);
    }
    public static void LogError(String tag, String words){
        HiLogLabel hiLogLabel = new HiLogLabel(3,0,tag);
        HiLog.error(hiLogLabel,words,"asd");
    }
    public static void isLoggable(String tag, String words){
        HiLogLabel hiLogLabel = new HiLogLabel(3,0,tag);
        HiLog.error(hiLogLabel,words,"asd");
    }
}
