package com.frosquivel.magicalcamera.provider;

import ohos.agp.components.*;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ohos.agp.components.ComponentContainer.LayoutConfig.MATCH_PARENT;

public class PagerProvider extends PageSliderProvider {
    private List<Integer> pagers = new ArrayList<>();
    private Context context;

    public PagerProvider(Context context, List<Integer> pagers) {
        this.context = context;
        this.pagers = pagers;
    }

    @Override
    public int getCount() {
        return pagers.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        Image image = new Image(context);
        try {
            ImageSource.SourceOptions options = new ImageSource.SourceOptions();
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            ResourceManager manager = context.getResourceManager();
            String path = manager.getMediaPath((Integer) pagers.get(i));
            Resource r1 = manager.getRawFileEntry(path).openRawFile();
            ImageSource source = ImageSource.create(r1, options);
            PixelMap p1 = source.createPixelmap(decodingOptions);
            image.setPixelMap(p1);
            image.setScaleMode(Image.ScaleMode.STRETCH);
            image.setLayoutConfig(new DirectionalLayout.LayoutConfig(MATCH_PARENT, MATCH_PARENT));
            componentContainer.addComponent(image);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        return image;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }
}
