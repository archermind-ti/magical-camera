package com.frosquivel.magicalcamera;

import com.frosquivel.magicalcamera.slice.SettingAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SettingsMainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SettingAbilitySlice.class.getName());
    }

}
