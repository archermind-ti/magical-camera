package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.Utilities.ConvertSimpleImage;
import com.frosquivel.magicalcamera.util.Const;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;
import ohos.media.image.PixelMap;


public class ImageViewAbilitySlice extends AbilitySlice {
    private Image image;
    private PixelMap pixelMap;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_image);
        image = (Image) findComponentById(ResourceTable.Id_image);
        pixelMap = intent.getSequenceableParam(Const.INTENT_PATH);
        image.setPixelMap(pixelMap);

        findComponentById(ResourceTable.Id_resize).setClickedListener(v -> {
            byte[] arrayBytesFromBitmap = ConvertSimpleImage.bitmapToBytes(pixelMap);
            PixelMap map = ConvertSimpleImage.resizeImageRunTime(arrayBytesFromBitmap,
                    image.getPixelMap().getImageInfo().size.width - 150,
                    image.getPixelMap().getImageInfo().size.height - 150, false);
            image.setPixelMap(map);
        });
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
