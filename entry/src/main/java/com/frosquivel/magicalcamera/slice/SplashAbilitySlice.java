package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.MainAbility;
import com.frosquivel.magicalcamera.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;

public class SplashAbilitySlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_splash);

        TaskDispatcher dispatcher = createSerialTaskDispatcher("timeout", TaskPriority.DEFAULT);
        dispatcher.delayDispatch(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(MainAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                terminateAbility();
            }
        }, 3000);
    }

    private void delayTask() {

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
