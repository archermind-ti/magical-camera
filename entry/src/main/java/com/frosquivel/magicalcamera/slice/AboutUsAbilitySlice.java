package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.util.Const;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;


public class AboutUsAbilitySlice extends AbilitySlice {


    private Button btnCreator, btnContributor1, btnContributor2;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_about_us);
        btnCreator = (Button) findComponentById(ResourceTable.Id_btncreator);
        btnContributor1 = (Button) findComponentById(ResourceTable.Id_btncontributor1);
        btnContributor2 = (Button) findComponentById(ResourceTable.Id_btncontributor2);

        btnCreator.setClickedListener(v -> {
            gotoWeb(getString(ResourceTable.String_CREATOR));
        });
        btnContributor1.setClickedListener(v -> {
            gotoWeb(getString(ResourceTable.String_CONTRIBUTOR1));
        });
        btnContributor2.setClickedListener(v -> {
            gotoWeb(getString(ResourceTable.String_CONTRIBUTOR2));
        });
    }

    private void gotoWeb(String url) {
        present(new WebViewAbilitySlice(), new Intent().setParam(Const.INTENT_URL, url));
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
