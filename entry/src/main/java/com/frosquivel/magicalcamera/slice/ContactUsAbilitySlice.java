package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.provider.PagerProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.PageSlider;
import ohos.agp.components.PageSliderIndicator;
import ohos.agp.components.element.ShapeElement;

import java.util.ArrayList;
import java.util.List;


public class ContactUsAbilitySlice extends AbilitySlice {
    private PageSlider pageSlider;
    private PageSliderIndicator pageSliderIndicator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_contact_us);
        initPageSlider();
        initIndicator();
    }

    private void initIndicator() {
        pageSliderIndicator = (PageSliderIndicator) findComponentById(ResourceTable.Id_indicator);
        PageSliderIndicator indicator = (PageSliderIndicator)findComponentById(ResourceTable.Id_indicator);
        ShapeElement normalElement = new ShapeElement();
        normalElement.setRgbColor(RgbColor.fromArgbInt(0xFFFFFF));
        normalElement.setAlpha(168);
        normalElement.setShape(ShapeElement.OVAL);
        normalElement.setBounds(0, 0, 32, 32);
        ShapeElement selectedElement = new ShapeElement();
        selectedElement.setRgbColor(RgbColor.fromArgbInt(0xFFFFFF));
        selectedElement.setAlpha(250);
        selectedElement.setShape(ShapeElement.OVAL);
        selectedElement.setBounds(0, 0, 48, 48);
        indicator.setItemElement(normalElement, selectedElement);
        indicator.setItemOffset(20);
        indicator.setPageSlider(pageSlider);
    }

    private void initPageSlider() {
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_page_slider);

        List<Integer> datas = new ArrayList<>();
        datas.add(ResourceTable.Media_ic_contact_us_1);
        datas.add(ResourceTable.Media_ic_contact_us_2);
        datas.add(ResourceTable.Media_ic_contact_us_3);
        datas.add(ResourceTable.Media_ic_contact_us_4);
        datas.add(ResourceTable.Media_ic_contact_us_5);
        datas.add(ResourceTable.Media_ic_contact_us_6);
        pageSlider.setProvider(new PagerProvider(getAbility(),datas));
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
