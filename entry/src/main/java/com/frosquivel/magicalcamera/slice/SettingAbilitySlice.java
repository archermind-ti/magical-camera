package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.util.Utils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;


public class SettingAbilitySlice extends AbilitySlice {
    private TextField photoName, directoryName;
    private Slider slider;
    private RadioContainer radioContainer;
    private Switch mSwitch;
    private Text quality;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_setting);

        photoName = (TextField) findComponentById(ResourceTable.Id_photoname);
        directoryName = (TextField) findComponentById(ResourceTable.Id_directoryname);
        radioContainer = (RadioContainer) findComponentById(ResourceTable.Id_radiocontainer);
        slider = (Slider) findComponentById(ResourceTable.Id_sliderquality);
        mSwitch = (Switch) findComponentById(ResourceTable.Id_btn_switch);
        quality = (Text) findComponentById(ResourceTable.Id_textquality);
        valuesSharedPreference();
        listeners();

    }

    private void valuesSharedPreference() {
        directoryName.setText(Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_DIRECTORY_NAME));
        photoName.setText(Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_PHOTO_NAME));
        slider.setProgressValue(Integer.parseInt(
                Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_QUALITY_PICTURE).equals("")
                        ? "0" : Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_QUALITY_PICTURE)));
        quality.setText(Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_QUALITY_PICTURE));

        if (Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_FORMAT).equals(Utils.C_JPEG)) {
            RadioButton radioButton = (RadioButton) radioContainer.getComponentAt(0);
            radioButton.setChecked(true);
        } else if (Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_FORMAT).equals(Utils.C_PNG)) {
            RadioButton radioButton = (RadioButton) radioContainer.getComponentAt(1);
            radioButton.setChecked(true);
        } else if (Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_FORMAT).equals(Utils.C_WEBP)) {
            RadioButton radioButton = (RadioButton) radioContainer.getComponentAt(2);
            radioButton.setChecked(true);
        }


        mSwitch.setChecked(Boolean.parseBoolean(Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_AUTO_IC_NAME)));
    }

    private void listeners() {
        photoName.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                Utils.setSharedPreference(getContext(), Utils.C_PREFERENCE_MC_PHOTO_NAME, photoName.getText());
            }
        });
        directoryName.addTextObserver(new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                Utils.setSharedPreference(getContext(), Utils.C_PREFERENCE_MC_DIRECTORY_NAME, directoryName.getText());
            }
        });
        slider.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                quality.setText(i + "");
                Utils.setSharedPreference(getContext(), Utils.C_PREFERENCE_MC_QUALITY_PICTURE, String.valueOf(i));
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });
        mSwitch.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                Utils.setSharedPreference(getContext(), Utils.C_PREFERENCE_MC_AUTO_IC_NAME, String.valueOf(b));
            }
        });

        radioContainer.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                RadioButton radioButton = (RadioButton) radioContainer.getComponentAt(i);
                String format = Utils.C_JPEG;
                if (radioButton.getText().toLowerCase().equals(Utils.C_JPEG)) {
                    format = Utils.C_JPEG;
                } else if (radioButton.getText().toLowerCase().equals(Utils.C_PNG)) {
                    format = Utils.C_PNG;
                } else if (radioButton.getText().toLowerCase().equals(Utils.C_WEBP)) {
                    format = Utils.C_WEBP;
                }
                Utils.setSharedPreference(getContext(), Utils.C_PREFERENCE_MC_FORMAT, format);
            }
        });

    }


    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
