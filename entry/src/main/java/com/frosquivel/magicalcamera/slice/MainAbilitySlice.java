package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.MagicalCamera;
import com.frosquivel.magicalcamera.MagicalPermissions;
import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.SettingsMainAbility;
import com.frosquivel.magicalcamera.Utilities.ConvertSimpleImage;
import com.frosquivel.magicalcamera.util.Const;
import com.frosquivel.magicalcamera.util.Utils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.dialog.ToastDialog;


public class MainAbilitySlice extends AbilitySlice {
    private MagicalCamera magicalCamera;
    //The pixel percentage is declare like an percentage of 100, if your value is 50, the photo will have the middle quality of your camera.
// this value could be only 1 to 100.
    private int RESIZE_PHOTO_PIXELS_PERCENTAGE = 80;

    private String[] permissions = {
            "ohos.permission.WRITE_EXTERNAL_STORAGE",
            "ohos.permission.READ_EXTERNAL_STORAGE",
            "ohos.permission.CAMERA",
            "ohos.permission.MEDIA_LOCATION",
            "ohos.permission.READ_MEDIA",
            "ohos.permission.WRITE_MEDIA",
    };

    private Image image, btnMenu;
    private Button btnTakePhoto, btnSelectedPhoto, btnSavePhoto, btnMore;
    private int whirlCount = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        MagicalPermissions magicalPermissions = new MagicalPermissions(this, permissions);
        magicalCamera = new MagicalCamera(this, RESIZE_PHOTO_PIXELS_PERCENTAGE, magicalPermissions);
        findComponentView();
        setClickedListener();

        Utils.setInitialSharedPreference(getAbility(), false);
    }

    private void setClickedListener() {
        btnTakePhoto.setClickedListener(v -> {
            magicalCamera.takePhoto();
        });
        btnSelectedPhoto.setClickedListener(v -> {
            magicalCamera.selectedPicture("Select Picture");
        });
        btnSavePhoto.setClickedListener(v -> {
            //if you need save your bitmap in device use this method and return the path if you need this
            //You need to send, the bitmap picture, the photo name, the directory name, the picture type, and autoincrement photo name if           //you need this send true, else you have the posibility or realize your standard name for your pictures.
            String path = magicalCamera.savePhotoInMemoryDevice(magicalCamera.getPhoto(),
                    Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_PHOTO_NAME),
                    Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_DIRECTORY_NAME),
                    Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_FORMAT),
                    Boolean.parseBoolean(Utils.getSharedPreference(getContext(), Utils.C_PREFERENCE_MC_AUTO_IC_NAME)));

            DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(this)
                    .parse(ResourceTable.Layout_layout_toast, null, false);
            Text text = (Text) toastLayout.findComponentById(ResourceTable.Id_text_content);
            String textStr = path != null ? ("The photo is save in device, please check this path: " + path) : "Sorry your photo dont write in devide, please contact with fabian7593@gmail and say this error";
            text.setText(textStr);
            new ToastDialog(getContext())
                    .setComponent(toastLayout)
                    .setAlignment(LayoutAlignment.BOTTOM)
                    .show();
        });

        btnMore.setClickedListener(v -> {
            showDialoglist(btnMore);
        });

        image.setClickedListener(v -> {
            if (magicalCamera != null && magicalCamera.getPhoto() != null) {
                //The static method of bitmap is usage because if you pass string base 64 or
                //array bytes like intent parameter bundle, the app have an error :(
                //This error is E/JavaBinder﹕ !!! FAILED BINDER TRANSACTION !!!
                //please check this link
                //http://stackoverflow.com/questions/31708092/setting-a-bitmap-as-intent-extra-causes-error

                present(new ImageViewAbilitySlice(), new Intent().setParam(Const.INTENT_PATH, image.getPixelMap()));

            } else {
                DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(this)
                        .parse(ResourceTable.Layout_layout_toast, null, false);
                Text textContent = (Text) toastLayout.findComponentById(ResourceTable.Id_text_content);
                textContent.setText("Your image is null, please select or take one");
                new ToastDialog(this)
                        .setAlignment(LayoutAlignment.BOTTOM)
                        .setComponent(toastLayout)
                        .show();
            }
        });
        btnMenu.setClickedListener(view -> {
            PopupDialog popupDialog = new PopupDialog(this, view);
            String[] titles = {"Convert to String base 64", "Photo/Image Information", "Rotate Photo/Image 90º"};
            DirectionalLayout toastLayout = new DirectionalLayout(this);
            toastLayout.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
            toastLayout.setWidth(ComponentContainer.LayoutConfig.MATCH_CONTENT);
            toastLayout.setOrientation(Component.VERTICAL);
            for (String name : titles) {
                Text text = new Text(this);
                text.setText(name);
                text.setTextSize(50);
                text.setPadding(40, 20, 40, 20);
                text.setClickedListener(v -> {
                    popupDialog.hide();
                    onMenuPopUpDialogitemClick(name);
                });
                toastLayout.addComponent(text);
            }

            popupDialog.setCustomComponent(toastLayout);
            popupDialog.show();
        });

    }

    private void findComponentView() {
        image = (Image) findComponentById(ResourceTable.Id_img);
        btnTakePhoto = (Button) findComponentById(ResourceTable.Id_takephoto);
        btnSelectedPhoto = (Button) findComponentById(ResourceTable.Id_selectedphoto);
        btnSavePhoto = (Button) findComponentById(ResourceTable.Id_savephoto);
        btnMore = (Button) findComponentById(ResourceTable.Id_more);
        btnMenu = (Image) findComponentById(ResourceTable.Id_menu);

    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultCode != -1 || resultData == null) {
            return;
        }
        if (requestCode == MagicalCamera.SELECT_PHOTO) {
            magicalCamera.resultPhoto(requestCode, resultCode, resultData);
            image.setPixelMap(magicalCamera.getPhoto());
            btnSavePhoto.setVisibility(Component.VISIBLE);
            btnMenu.setVisibility(Component.VISIBLE);
        } else if (requestCode == MagicalCamera.TAKE_PHOTO) {
            magicalCamera.resultPhoto(requestCode, resultCode, resultData);
            image.setPixelMap(magicalCamera.getPhoto());
            btnSavePhoto.setVisibility(Component.VISIBLE);
            btnMenu.setVisibility(Component.VISIBLE);
        }

    }

    private void showDialoglist(Component component) {
        PopupDialog popupDialog = new PopupDialog(this, component);
        String[] titles = {"Github", "Settings", "Documentation", "TODO List", "Help Us", "Contact Us", "About Developers"};
        DirectionalLayout toastLayout = new DirectionalLayout(this);
        toastLayout.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        toastLayout.setWidth(ComponentContainer.LayoutConfig.MATCH_CONTENT);
        toastLayout.setOrientation(Component.VERTICAL);
        for (String name : titles) {
            Text text = new Text(this);
            text.setText(name);
            text.setTextSize(50);
            text.setPadding(40, 20, 40, 20);
            text.setClickedListener(v -> {
                popupDialog.hide();
                onPopUpDialogitemClick(name);
            });
            toastLayout.addComponent(text);
        }

        popupDialog.setCustomComponent(toastLayout);
        popupDialog.show();

    }

    private void onPopUpDialogitemClick(String title) {
        String url = "";
        AbilitySlice abilitySlice = new WebViewAbilitySlice();
        switch (title) {
            case "Github":
                url = getString(ResourceTable.String_GITHUB);
                break;
            case "Documentation":
                url = getString(ResourceTable.String_DOCUMENTATION);
                break;
            case "TODO List":
                url = getString(ResourceTable.String_TODO_LIST);
                break;
            case "About Developers":
                abilitySlice = new AboutUsAbilitySlice();
                break;
            case "Help Us":
                abilitySlice = new HelpUsAbilitySlice();
                break;
            case "Contact Us":
                abilitySlice = new ContactUsAbilitySlice();
                break;
            case "Shared":
                return;
            case "Settings":
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(getBundleName())
                        .withAbilityName(SettingsMainAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                return;
            default:
                break;
        }
        present(abilitySlice, new Intent().setParam(Const.INTENT_URL, url));

    }

    private void onMenuPopUpDialogitemClick(String title) {
        switch (title) {
            case "Convert to String base 64": {
                String base64 = "";
                try {
                    //convert the bitmap to bytes array
                    byte[] bytesArray = ConvertSimpleImage.bitmapToBytes(magicalCamera.getPhoto());
                    //convert the bytes to string 64, with this form is easly to send by web service or store data in DB
                    String imageBase64 = ConvertSimpleImage.bytesToStringBase64(bytesArray);

                    /*****************************************************************
                     *                    Revert the process
                     *****************************************************************/
                    //if you need to revert the process
                    //byte[] anotherArrayBytes = ConvertSimpleImage.stringBase64ToBytes(imageBase64);
                    //again deserialize the image
                    //Bitmap myImageAgain = ConvertSimpleImage.bytesToBitmap(anotherArrayBytes);

                    base64 = imageBase64.substring(0, 300);


                } catch (Exception e) {
                    e.printStackTrace();
                }


                DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(this)
                        .parse(ResourceTable.Layout_layout_base64_toast, null, false);
                Text textContent = (Text) toastLayout.findComponentById(ResourceTable.Id_text_content);
                textContent.setText(base64);
                new ToastDialog(getContext())
                        .setComponent(toastLayout)
                        .setAutoClosable(false)
                        .setAlignment(LayoutAlignment.CENTER)
                        .show();
            }
            break;
            case "Photo/Image Information": {
                if (magicalCamera.initImageInformation()) {
                    String text = "Latitude: " + magicalCamera.getPrivateInformation().getLatitude()
                            + "\nLongitude: " + magicalCamera.getPrivateInformation().getLongitude()
                            + "\nAltitude: " + magicalCamera.getPrivateInformation().getAltitude()
                            + "\nImage Hight: " + magicalCamera.getPrivateInformation().getImageHeight()
                            + "\nImage Width: " + magicalCamera.getPrivateInformation().getImageWidth();
                    DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(this)
                            .parse(ResourceTable.Layout_layout_toast, null, false);
                    Text textContent = (Text) toastLayout.findComponentById(ResourceTable.Id_text_content);
                    textContent.setText(text);
                    new ToastDialog(getContext())
                            .setComponent(toastLayout)
                            .setAlignment(LayoutAlignment.CENTER)
                            .show();
                }
            }
            break;
            case "Rotate Photo/Image 90º":
                //for once click rotate the picture in 90ª, and set in the image view.
                whirlCount++;
                magicalCamera.setPhoto(magicalCamera.rotatePicture(magicalCamera.getPhotoImageSource(), MagicalCamera.ORIENTATION_ROTATE_90 * whirlCount));
                //its need the ui thread, but in this case we use in on Post Execute
                image.setPixelMap(magicalCamera.getPhoto());
                break;
            default:
                break;
        }
    }
}
