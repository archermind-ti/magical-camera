package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.util.Const;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Image;


public class HelpUsAbilitySlice extends AbilitySlice {
    private Image imgEmail, imgGithub, imgShare, imgYouTube;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_help_us);
        imgEmail = (Image) findComponentById(ResourceTable.Id_imgEmail);
        imgGithub = (Image) findComponentById(ResourceTable.Id_imgGithub);
        imgShare = (Image) findComponentById(ResourceTable.Id_imgShare);
        imgYouTube = (Image) findComponentById(ResourceTable.Id_imgYouTube);

        imgEmail.setClickedListener(v -> {
        });
        imgGithub.setClickedListener(v -> {
            gotoWeb(getString(ResourceTable.String_GITHUB));
        });
        imgShare.setClickedListener(v -> {
        });
        imgYouTube.setClickedListener(v -> {
            gotoWeb(getString(ResourceTable.String_YOUTUBE));
        });
    }

    private void gotoWeb(String url) {
        present(new WebViewAbilitySlice(), new Intent().setParam(Const.INTENT_URL, url));
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
