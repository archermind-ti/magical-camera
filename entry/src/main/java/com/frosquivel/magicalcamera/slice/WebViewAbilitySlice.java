package com.frosquivel.magicalcamera.slice;

import com.frosquivel.magicalcamera.ResourceTable;
import com.frosquivel.magicalcamera.util.Const;
import com.frosquivel.magicalcamera.util.LogUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.webengine.WebView;


public class WebViewAbilitySlice extends AbilitySlice {
    private WebView mWebView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_web);
        mWebView = (WebView) findComponentById(ResourceTable.Id_webview);
        LogUtils.LogError("URL",intent.getStringParam(Const.INTENT_URL));
        mWebView.load(intent.getStringParam(Const.INTENT_URL));
    }

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
